﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace ThaiBulkSms.API
{
    public class SmsService
    {
        private SmsData sms;
        public SmsService(SmsData sms)
        {
            this.sms = sms;
            Validate();
        }

        public void Validate()
        {
           var errors = new List<string>();
            if(string.IsNullOrEmpty(sms.username))
            {
                errors.Add("\nusername is required");
            }

            if (string.IsNullOrEmpty(sms.password))
            {
                errors.Add("\npassword is required");
            }

            if (sms.msisdn.Count == 0)
            {
                errors.Add("\nDestination Phone Number need at least one number");
            }

            if (string.IsNullOrEmpty(sms.message))
            {
                errors.Add("\nPlease set some message");
            }

            if(errors.Count >0)
            {
                var errorMessage = string.Join(" ", errors.ToArray());
                throw new Exception(errorMessage);
            }
        }

        public SMS Send()
        {

            var request =
                WebRequest.Create("http://www.thaibulksms.com/sms_api.php");

            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            var postData =
               CreatePostData();
            var byteArray = Encoding.UTF8.GetBytes(postData);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            var dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            var response = request.GetResponse();

            // Display the status.
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();


            XmlSerializer serializer =
                new XmlSerializer(typeof(SMS));
            SMS result =
                (SMS)serializer.Deserialize(dataStream);
            
            // Clean up the streams.
            //reader.Close();
            dataStream.Close();
            response.Close();
            return result;
        }

        private string CreatePostData()
        {
 
            var smsDataType = sms.GetType();

            var properties = smsDataType.GetProperties();
            var postData = new List<string>();
            foreach (var propery in properties)
            {
                if (propery.GetValue(sms, null) == null)
                {
                    continue;
                }

                if (propery.PropertyType == typeof(List<string>))
                {
                    var value =
                        (List<string>)propery.GetValue(sms, null);

                    postData.Add(string.Format("{0}={1}",
                   propery.Name, string.Join(",", value.ToArray())));
                }
                else if (propery.PropertyType == typeof(DateTime))
                {

                    var value = (DateTime)propery.GetValue(sms, null);
                    postData.Add(string.Format("{0}={1}",
                   propery.Name, value.ToString("yyMMddHHmm")));
                }
                else
                {
                    postData.Add(string.Format("{0}={1}",
                   propery.Name, propery.GetValue(sms, null)));
                }
            }

            return string.Join("&", postData.ToArray());
        }
    }
}
