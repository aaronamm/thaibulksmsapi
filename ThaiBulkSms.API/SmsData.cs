﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThaiBulkSms.API
{
    public class SmsData
    {
        public string username { get; set; }
        public string password { get; set; }
        public List<string> msisdn { get; set; }
        public string message { get; set; }
        public string sender { get; set; }
        public DateTime ScheduledDelivery { get; set; }
        public Force force { get; set; }

        public SmsData()
        {
            msisdn = new List<string>();
            ScheduledDelivery = DateTime.Now;
            force = Force.standard;
        }

    }

    public enum Force
    {
        standard,
        premium 
    }
}
