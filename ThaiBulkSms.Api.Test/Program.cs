﻿using System;
using ThaiBulkSms.API;

namespace ThaiBulkSms.Api.Test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            switch (args[0])
            {
                case "1":
                    SendSms(args);
                    break;
                case "2":
                    //check sms remain
                    break;
            }

        }//end main method

        public static void SendSms(string[] args)
        {
            var sms = new SmsData();
            sms.username = args[0]; //หมายเลขโทรศัพท์ที่สมัครกับ ThaiBulkSms
            sms.password = args[1]; //รหัสผ่าน
            sms.msisdn.Add(args[2]); //หมายเลขโทรศัพท์ที่ต้องการส่ง เพิ่มได้
            //sms.msisdn.AddRange(new[] {"",""}); ใส่เป็น list  
            sms.message = "good night"; //ข้อความที่ต้องการส่ง

            //sms.ScheduledDelivery = DateTime.Now.AddHours(5);//ตั้งเวลาส่ง sms ล่วงหน้า
            var service = new SmsService(sms); //สร้าง SmsService object
            var result = service.Send(); //ส่ง sms 

            //ตรวจสอบผลลัพธ์ที่ได้จากการส่ง sms
            if (result.Status == "0") //มีความผิดพลาดเกิดขึ้นในการส่ง sms
            {
                Console.WriteLine(result.Status);
                Console.WriteLine(result.Detail);
            }
            else //ส่ง sms เรียบร้อย
            {
                Console.WriteLine("send sms successfully");

                if (result.QUEUE != null)
                {
                    foreach (var queue in result.QUEUE) //แสดงผลลัพธ์ที่ส่งกลับมา
                    {
                        Console.WriteLine("Msisdn = {0}",
                                          queue.Msisdn); //หมายเลขโทรศัพท์ที่ส่ง sms
                        Console.WriteLine("Status = {0}",
                                          queue.Status); //สถานะการส่ง 1 = OK, 0 = Fail
                        Console.WriteLine("Transaction = {0}",
                                          queue.Transaction);
                        //Transaction each destination
                        Console.WriteLine("UsedCredit = {0}",
                                          queue.UsedCredit); //จำนวน sms ที่ใช้ไป
                        Console.WriteLine("RemainCredit = {0}",
                                          queue.RemainCredit); //จำนวน sms คงเหลือ ที่ยังส่งได้
                    }
                }
            }

        }//end method

    }
}
